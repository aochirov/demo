package ao.demo.pipeline;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ao.demo.pipeline.handler.LmeDataHandler;
import ao.demo.pipeline.handler.PrimeDataHandler;

/**
 * The <code>PipelineTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class LmePipelineTest extends DataPipeline {

    /**
     * The <code>LmeTestHandler</code> is TODO write correct comment
     *
     * @author  Andrey OCHIROV
     * @version 1.0
     */
    private class LmeTestHandler extends LmeDataHandler {

        /* (non-Javadoc)
         * @see ao.demo.pipeline.handler.LmeDataHandler#handle(ao.demo.pipeline.DataEntry)
         */
        @Override
        public Integer handle(DataEntry entry) throws Exception {
            Path path = entry.getFile();
            Path newPath = path.subpath(0, 1).resolve("output").resolve(
                path.subpath(path.getNameCount() - 1, path.getNameCount()));

            try {
                Utils.lock(path.toString());

                int code = super.handle(entry);

                ((TestDataEntry)entry).setResult(Files.readAllBytes(newPath));
                return code;
            } finally {
                Utils.unlock(path.toString());
            }
        }

    }

    /**
     * The <code>TestDataEntry</code> is TODO write correct comment
     *
     * @author  Andrey OCHIROV
     * @version 1.0
     */
    private class TestDataEntry extends DataEntry {

        private byte[] result;
        private byte[] reference;

        /**
         * Creates an instance of class <code>TestDataEntry</code>.
         *
         * @param   file
         * @param   reference
         */
        public TestDataEntry(Path file, byte[] reference) {
            super(file);

            this.reference = reference;
        }

        /**
         * @return 	the reference
         */
        public byte[] getReference() {
            return reference;
        }

        /**
         * @return  the result
         */
        public byte[] getResult() {
            return result;
        }

        /**
         * @param 	result the result to set
         */
        public void setResult(byte[] result) {
            this.result = result;
        }

    }

    private AtomicInteger counter = new AtomicInteger();

    private Path root;
    private Path input;
    private Path output;
    private Path outputRoot;

    private String INPUT_STRING =
        "| LAST_TRADING_DATE          | DELIVERY_DATE | MARKET              | LABEL                                |\n"
            + "| %02d-%02d-2018            | %02d-%02d-2018    | PB              | Lead 13 March 2018               |";

    private String RESULT_STRING =
        "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |\n"
            + "| %02d-%02d-2018 | %02d-%02d-2018 | PB | Lead 13 March 2018 | TRUE |\n";

    /**
     * Creates an instance of class <code>PipelineTest</code>.
     */
    public LmePipelineTest() {
        super(Logger.getLogger(LmePipelineTest.class.getName()));

        logger.setLevel(Level.OFF);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e1) {
        }

        destroy();

        Path archive = Paths.get("data", "archive", "LME");

        archive.toFile().getParentFile().deleteOnExit();
        archive.toFile().deleteOnExit();

        try {
            Files.list(archive).forEach(x -> {
                x.toFile().deleteOnExit();
            });
        } catch (Exception e) {
        }
    }

    /**
     * @throws  Exception
     */
    @Before
    public void tearUp() throws Exception {
        root = Paths.get("data", "LME");
        outputRoot = Paths.get("data", "output");

        File rootFile = root.toFile();
        Assert.assertTrue(rootFile.isDirectory() || rootFile.mkdirs());

        create();

        root.toFile().deleteOnExit();
        outputRoot.toFile().deleteOnExit();
    }

    /**
     *
     */
    @Test
    public void testCreateHandlers() {
        List<DataHandler> handlers = super.createHandlers();

        Assert.assertTrue(2 == handlers.size());
        Assert.assertTrue(handlers.get(0).getClass() == LmeDataHandler.class);
        Assert.assertTrue(handlers.get(1).getClass() == PrimeDataHandler.class);
    }

    /**
     * @throws  Exception
     */
    @Test
    public void testPipeline() throws Exception {
        int frame = 100;
        List<TestDataEntry> entries = new ArrayList<>(frame);

        for (int i = 0; i < frame; i++) {
            entries.add(makeDataEntry());

            process(entries.get(i).list());
        }

        Future<Integer> f = process(new DataEntry(Paths.get("pipeline", "pipelineTestPoint")).list());

        try {
            f.get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            Assert.fail("Failed during pipeline test with next message: " + e.getMessage());
        }

        for (TestDataEntry entry : entries) {
            Assert.assertArrayEquals(entry.getResult(), entry.getReference());
        }
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.DataPipeline#createHandlers()
     */
    @Override
    protected List<DataHandler> createHandlers() {
        List<DataHandler> handlers = new ArrayList<>();

        handlers.add(new LmeTestHandler());
        handlers.add(new DataHandler() {

            @Override
            public Integer handle(DataEntry entry) throws Exception {
                if (100 == counter.get()) {
                    return 0;
                } else {
                    return 1;
                }
            }

            @Override
            public boolean isSupported(DataEntry entry) {
                return "pipelineTestPoint".equals(entry.getType());
            }

        });

        return handlers;
    }

    /**
     *
     * @throws  Exception
     */
    private TestDataEntry makeDataEntry() throws Exception {
        int month = 0;

        while (true) {
            month = (int)(1 + Math.random() * 11);
            String name = String.format("PB_%02d_2018.txt", month);

            input = root.resolve(name);
            output = outputRoot.resolve(name);

            if (Files.notExists(input)) {
                break;
            }

            try {
                TimeUnit.MILLISECONDS.sleep(month);
            } catch (InterruptedException e) {}
        }

        int day = (int)(1 + Math.random() * 27);
        String testData = String.format(INPUT_STRING, day, month, day + 3, month);
        String resultData = String.format(RESULT_STRING, day, month, day + 3, month);

        Files.write(input, testData.getBytes(), StandardOpenOption.CREATE,
            StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

        input.toFile().deleteOnExit();
        output.toFile().deleteOnExit();

        return new TestDataEntry(input, resultData.getBytes());
    }

}