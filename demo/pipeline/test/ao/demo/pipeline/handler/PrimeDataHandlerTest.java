package ao.demo.pipeline.handler;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;

/**
 * The <code>PrimeDataHandlerTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class PrimeDataHandlerTest {

    private Path root;
    private Path input;
    private Path outputA;
    private Path outputB;
    private Path outputRoot;

    private DataHandler dh;

    private String INPUT_STRING =
        "|  LAST_TRADING_DATE | DELIVERY_DATE |  MARKET           | LABEL              | EXCHANGE_CODE | TRADABLE  |\n"
            + "|  14-03-2018     | 18-03-2018       |  PB       | Lead 13 March 2018     | PB_03_2018    | FALSE   |\n"
            + "|  14-04-2018     | 18-04-2018       |  PB       | Lead 14 April 2018     | PB_04_2018    | TRUE     |";

    private String RESULT_STRING_A =
        "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |\n"
            + "| 14-03-2018 | 18-03-2018 | PB | Lead 13 March 2018 | FALSE |\n";

    private String RESULT_STRING_B =
        "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |\n"
            + "| 14-04-2018 | 18-04-2018 | PB | Lead 14 April 2018 | TRUE |\n";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void tearUp() throws Exception {
        dh = new PrimeDataHandler();

        root = Paths.get("data", "PRIME");
        input = root.resolve("PRIME_PB_03_2018.txt");
        outputRoot = Paths.get("data", "LME", "PRIME");
        outputA = outputRoot.resolve("PB_03_2018.txt");
        outputB = outputRoot.resolve("PB_04_2018.txt");

        File rootFile = root.toFile();
        Assert.assertTrue(rootFile.isDirectory() || rootFile.mkdirs());

        Files.write(input, INPUT_STRING.getBytes(), StandardOpenOption.CREATE,
            StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

        root.toFile().deleteOnExit();
        input.toFile().deleteOnExit();
        outputRoot.toFile().getParentFile().deleteOnExit();
        outputRoot.toFile().deleteOnExit();
        outputA.toFile().deleteOnExit();
        outputB.toFile().deleteOnExit();
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.PrimeDataHandler#handle(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testHandle() {
        try {
            dh.handle(new DataEntry(input));

            Assert.assertArrayEquals(Files.readAllBytes(outputA), RESULT_STRING_A.getBytes());
            Assert.assertArrayEquals(Files.readAllBytes(outputB), RESULT_STRING_B.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Not yet implemented");
        }
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.PrimeDataHandler#isSupported(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testIsSupported() {
        Assert.assertTrue(dh.isSupported(new DataEntry(Paths.get("data", "PRIME", "correct.txt"))));

        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "prime", "correct.txt"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "PRIME", "correct.txtx"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "PRIMEX", "correct"))));
    }

}