package ao.demo.pipeline.handler;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;

/**
 * The <code>CmdDataHandlerTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class CmdDataHandlerTest {

    private Path root;
    private Path input;

    private DataHandler dh;

    private String INPUT_STRING = "echo Hello, World!\ncd .\ncd X:\\C:\\";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void tearUp() throws Exception {
        dh = new CmdDataHandler();

        Logger.getLogger(dh.getClass().getName()).setLevel(Level.OFF);

        root = Paths.get("data", "command");
        input = root.resolve("cmds.txt");

        File rootFile = root.toFile();
        Assert.assertTrue(rootFile.isDirectory() || rootFile.mkdirs());

        root.toFile().deleteOnExit();
        input.toFile().deleteOnExit();
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.CmdDataHandler#handle(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testHandle() {
        try {
            Files.write(input, INPUT_STRING.getBytes(), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

            int code = dh.handle(new DataEntry(input));

            Assert.assertTrue(1 == code);
        } catch (Exception e) {
            fail("An error occurred during the test: " + e.getMessage());
        }
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.CmdDataHandler#isSupported(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testIsSupported() {
        Assert.assertTrue(dh.isSupported(new DataEntry(Paths.get("data", "command", "correct.txt"))));

        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "COMMAND", "correct.txt"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "command", "correct.txtx"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "COMMANDX", "correct"))));
    }

}