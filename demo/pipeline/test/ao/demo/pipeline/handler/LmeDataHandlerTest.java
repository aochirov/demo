package ao.demo.pipeline.handler;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;

/**
 * The <code>LmeDataHandlerTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class LmeDataHandlerTest {

    private Path root;
    private Path input;
    private Path output;
    private Path outputRoot;

    private DataHandler dh;

    private String INPUT_STRING =
        "| LAST_TRADING_DATE          | DELIVERY_DATE | MARKET              | LABEL                                |\n"
            + "| 16-03-2018                | 17-03-2018    | PB              | Lead 13 March 2018                   |";

    private String RESULT_STRING =
        "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |\n"
            + "| 16-03-2018 | 17-03-2018 | PB | Lead 13 March 2018 | TRUE |\n";

    private String INPUT_STRING_X =
        "| LAST_TRADING_DATE          | DELIVERY_DATE | MARKET              | LABEL               | TRADABLE      |\n"
            + "| 12-03-2018                | 15-03-2018    | PB              | Lead 13 March 2018    | FALSE      |";

    private String RESULT_STRING_X =
        "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |\n"
            + "| 16-03-2018 | 17-03-2018 | PB | Lead 13 March 2018 | FALSE |\n";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void tearUp() throws Exception {
        dh = new LmeDataHandler();

        root = Paths.get("data", "LME");
        input = root.resolve("LDH_PB_03_2018.txt");
        outputRoot = Paths.get("data", "output");
        output = outputRoot.resolve("LDH_PB_03_2018.txt");

        File rootFile = root.toFile();
        Assert.assertTrue(rootFile.isDirectory() || rootFile.mkdirs());

        root.toFile().deleteOnExit();
        input.toFile().deleteOnExit();
        outputRoot.toFile().deleteOnExit();
        output.toFile().deleteOnExit();
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.LmeDataHandler#handle(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testHandle() {
        try {
            Files.write(input, INPUT_STRING.getBytes(), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

            dh.handle(new DataEntry(input));

            Assert.assertArrayEquals(Files.readAllBytes(output), RESULT_STRING.getBytes());

            Files.write(input, INPUT_STRING_X.getBytes(), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

            dh.handle(new DataEntry(input));

            Assert.assertArrayEquals(Files.readAllBytes(output), RESULT_STRING_X.getBytes());

            Files.write(input, INPUT_STRING.getBytes(), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

            dh.handle(new DataEntry(input));

            Assert.assertArrayEquals(Files.readAllBytes(output), RESULT_STRING_X.getBytes());
        } catch (Exception e) {
            fail("An error occurred during the test: " + e.getMessage());
        }
    }

    /**
     * Test method for {@link ao.demo.pipeline.handler.LmeDataHandler#isSupported(ao.demo.pipeline.DataEntry)}.
     */
    @Test
    public void testIsSupported() {
        Assert.assertTrue(dh.isSupported(new DataEntry(Paths.get("data", "LME", "correct.txt"))));

        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "lme", "correct.txt"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "LME", "correct.txtx"))));
        Assert.assertFalse(dh.isSupported(new DataEntry(Paths.get("data", "LMEX", "correct"))));
    }

}