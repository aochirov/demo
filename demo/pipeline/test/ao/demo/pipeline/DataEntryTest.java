package ao.demo.pipeline;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The <code>DataEntryTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class DataEntryTest {

    /**
     * @throws  Exception
     */
    @Before
    public void tearUp() throws Exception {
        Path root = Paths.get("data", "LME");
        File rootFile = root.toFile();

        Assert.assertTrue(rootFile.isDirectory() || rootFile.mkdirs());

        rootFile.deleteOnExit();
        root.resolve("PB_03_2018.txt").toFile().deleteOnExit();
    }

    /**
     * Test method for {@link ao.demo.pipeline.DataEntry#checkSignature(java.io.File)}.
     *
     * @throws  Exception
     */
    @Test
    public void testCheckSignature() throws Exception {
        Path file = Paths.get("data", "LME", "PB_03_2018.txt");

        Files.createFile(file);

        String signature = "10:" + System.currentTimeMillis();
        DataEntry entry = new DataEntry(file, signature); // giving false signature

        assertFalse(entry.checkSignature(file.toFile()));

        entry.updateSingature();

        assertTrue(entry.checkSignature(file.toFile()));

        Files.delete(file);

        TimeUnit.SECONDS.sleep(1);

        Files.createFile(file);

        assertFalse(entry.checkSignature(file.toFile()));

        Files.delete(file);
    }

    /**
     * Test method for {@link ao.demo.pipeline.DataEntry#getFile()}.
     *
     * @throws  Exception
     */
    @Test
    public void testGetFile() throws Exception {
        Path file = Paths.get("data", "LME", "PB_03_2018.txt");

        DataEntry entry = new DataEntry(file);

        Files.createFile(file);

        assertEquals(Paths.get("data", "LME", "PB_03_2018.txt"), entry.getFile());

        Files.delete(file);
    }

    /**
     * Test method for {@link ao.demo.pipeline.DataEntry#getType()}.
     */
    @Test
    public void testGetType() {
        DataEntry entry = new DataEntry(Paths.get("data", "LME", "PB_03_2018.txt"));

        assertEquals("LME", entry.getType());
    }

    /**
     * Test method for {@link ao.demo.pipeline.DataEntry#list()}, {@link ao.demo.pipeline.DataEntry#isListed()} and
     * {@link ao.demo.pipeline.DataEntry#delist()}.
     */
    @Test
    public void testIsListed() {
        DataEntry entry = new DataEntry(Paths.get("data", "LME", "PB_03_2018.txt"));

        assertFalse(entry.isListed());
        assertTrue(entry.list().isListed());
        assertFalse(entry.delist().isListed());

    }

}