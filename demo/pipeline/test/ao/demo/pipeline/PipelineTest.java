package ao.demo.pipeline;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The <code>PipelineTest</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class PipelineTest extends DataPipeline {

    /**
     * The <code>PipelineTestHandler</code> is TODO write correct comment
     *
     * @author  Andrey OCHIROV
     * @version 1.0
     */
    private class PipelineTestHandler implements DataHandler {

        /* (non-Javadoc)
         * @see ao.demo.pipeline.DataHandler#handle(ao.demo.pipeline.DataEntry)
         */
        @Override
        public Integer handle(DataEntry entry) throws Exception {
            counter.incrementAndGet();
            return 0;
        }

        /* (non-Javadoc)
         * @see ao.demo.pipeline.DataHandler#isSupported(ao.demo.pipeline.DataEntry)
         */
        @Override
        public boolean isSupported(DataEntry entry) {
            return "pipelineTest".equals(entry.getType());
        }

    }

    private AtomicInteger counter = new AtomicInteger();

    /**
     * Creates an instance of class <code>PipelineTest</code>.
     */
    public PipelineTest() {
        super(Logger.getLogger(PipelineTest.class.getName()));

        logger.setLevel(Level.OFF);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        destroy();
    }

    /**
     *
     */
    @Before
    public void tearUp() {
        create();
    }

    /**
     *
     */
    @Test
    public void testPipeline() {
        for (int i = 0; i < 100; i++) {
            DataEntry entry = new DataEntry(Paths.get("pipeline", "pipelineTest"));
            process(i % 2 == 0 ? entry.list() : entry);
        }

        Future<Integer> f = process(new DataEntry(Paths.get("pipeline", "pipelineTestPoint")).list());

        try {
            Assert.assertTrue(0 == f.get());
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            Assert.fail("Failed during pipeline test with next message: " + e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.DataPipeline#createHandlers()
     */
    @Override
    protected List<DataHandler> createHandlers() {
        List<DataHandler> handlers = new ArrayList<>();

        handlers.add(new PipelineTestHandler());
        handlers.add(new DataHandler() {

            @Override
            public Integer handle(DataEntry entry) throws Exception {
                if (50 == counter.get()) {
                    return 0;
                } else {
                    return 1;
                }
            }

            @Override
            public boolean isSupported(DataEntry entry) {
                return "pipelineTestPoint".equals(entry.getType());
            }

        });

        return handlers;
    }

}