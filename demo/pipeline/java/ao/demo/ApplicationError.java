package ao.demo;

/**
 * The <code>ApplicationError</code> is a type of of an error that could be thrown by application in case of critical
 * fault to indicate that application cannot continue to work.
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class ApplicationError extends Error {

    /**
     * The class finger print that is set to indicate serialization compatibility with a previous version of the class.
     */
    private static final long serialVersionUID = -4675725441153829581L;

    /**
     * The application error code for reporting to OS shell.
     */
    private int error;

    /**
     * Creates an instance of class <code>ApplicationError</code>.
     */
    public ApplicationError(int error) {
        this.error = error;
    }

    /**
     * Returns application's error code.
     *
     * @return the error code.
     */
    public int getErrorCode() {
        return error;
    }

}