package ao.demo.pipeline.handler;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;

/**
 * The <code>CmdDataHandler</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class CmdDataHandler implements DataHandler {

    private Logger logger = Logger.getLogger(getClass().getName());

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#handle(ao.demo.pipeline.DataEntry)
     */
    @Override
    public Integer handle(DataEntry entry) throws Exception {
        AtomicInteger code = new AtomicInteger();

        Files.lines(entry.getFile(), StandardCharsets.UTF_8)
            .filter(x -> null != x && !x.trim().isEmpty())
            .forEach(x -> {
                x = x.trim();

                Runtime r = Runtime.getRuntime();

                try {
                    logger.info(String.format("Running command: %s ...", x));

                    int errCode = r.exec(x).waitFor();

                    code.addAndGet(errCode);

                    if (0 == errCode) {
                        logger.info(String.format("Command '%s' executed successfully", x));
                    } else {
                        logger.warning(String.format("Command '%s' executed with error code: %04d", x, code));
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE,
                        String.format("An exception '%s' occurred while running command '%s' from entry '%s'",
                            e.getMessage(), x, entry.getFile()), e);
                }
            });
        return code.get();
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#isSupported(ao.demo.pipeline.DataEntry)
     */
    @Override
    public boolean isSupported(DataEntry entry) {
        return "command".equals(entry.getType()) && entry.getFile().toString().toLowerCase().endsWith(".txt");
    }

}