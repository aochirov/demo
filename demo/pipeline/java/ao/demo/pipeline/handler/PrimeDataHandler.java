package ao.demo.pipeline.handler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;
import ao.demo.pipeline.Utils;

/**
 * The <code>PrimeDataHandler</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class PrimeDataHandler implements DataHandler {

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#handle(ao.demo.pipeline.DataEntry)
     */
    @Override
    public Integer handle(DataEntry entry) throws Exception {
        Path path = entry.getFile();

        AtomicInteger line = new AtomicInteger(0);
        AtomicInteger column = new AtomicInteger(0);

        Map<String, Map<String, String[]>> rowMap = new HashMap<>();

        Files.lines(path, StandardCharsets.UTF_8)
            .filter(x -> null != x && !x.trim().isEmpty() && x.contains("|"))
            .forEach(x -> {
                String[] row = x.trim().split("\\|");

                if (0 == line.getAndIncrement()) {
                    for (int i = 0; i < row.length; i++) {
                        if ("EXCHANGE_CODE".equals(row[i].trim().toUpperCase())) {
                            column.set(i - 1);
                            break;
                        }
                    }
                    return;
                }

                if (7 == row.length) {
                    for (int i = 0; i < row.length - 1; i++) {
                        row[i] = row[i + 1].trim();
                    }

                    // getting map with all rows related to particular instrument...
                    Map<String, String[]> rows = rowMap.get(row[column.get()]);

                    if (null == rows) {
                        // if this first record for particular instrument we have to create a map
                        rowMap.put(row[column.get()], rows = new HashMap<>());
                    }
                    rows.put(row[2] + row[3], row);
                }
            });

        // separating rows from different instruments into different files
        rowMap.forEach((k, v) -> {
            Path newPath = path.subpath(0, 1).resolve("LME").resolve("PRIME").resolve(k + ".txt");

            // ensure that all required folders are created
            newPath.toFile().getParentFile().mkdirs();

            try {
                // lock path for current thread
                Utils.lock(newPath.toString());

                try (BufferedWriter bw = Files.newBufferedWriter(newPath, StandardCharsets.UTF_8,
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)) {
                    bw.write(String.format("| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |%n"));

                    v.values().forEach(x -> {
                        try {
                            x[4] = x[5]; // replace 'exchange_code' value with 'tradable' value

                            bw.write(String.format("| %s | %s | %s | %s | %S |%n", (Object[])x));
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                                String.format("An exception '%s' occurred while writing data '%s' into the file '%s'",
                                    e.getMessage(), Arrays.toString(x), newPath), e);
                       }
                    });
                } catch (IOException e) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                        String.format("An exception '%s' occurred while writing data into the file '%s'",
                            e.getMessage(), newPath), e);
               }
            } finally {
                Utils.unlock(newPath.toString());
            }
        });
        return 0;
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#isSupported(ao.demo.pipeline.DataEntry)
     */
    @Override
    public boolean isSupported(DataEntry entry) {
        return "PRIME".equals(entry.getType()) && entry.getFile().toString().toLowerCase().endsWith(".txt");
    }

}