package ao.demo.pipeline.handler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import ao.demo.pipeline.DataEntry;
import ao.demo.pipeline.DataHandler;
import ao.demo.pipeline.Utils;

/**
 * The <code>LmeDataHandler</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class LmeDataHandler implements DataHandler {

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#handle(ao.demo.pipeline.DataEntry)
     */
    @Override
    public Integer handle(DataEntry entry) throws Exception {
        Path path = entry.getFile();
        Path newPath = path.subpath(0, 1).resolve("output").resolve(
            path.subpath(path.getNameCount() - 1, path.getNameCount()));

        AtomicInteger line = new AtomicInteger(0);

        Map<String, String[]> rowMap = Files.lines(path, StandardCharsets.UTF_8)
            .filter(x -> line.getAndIncrement() > 0 && null != x && !x.trim().isEmpty() && x.contains("|"))
            .map(x -> {
                String[] row = x.trim().split("\\|");

                // shift and cleanup columns
                for (int i = 0; i < row.length - 1; i++) {
                    row[i] = row[i + 1].trim();
                }

                switch(row.length) {
                    case 5:
                        row[4] = "TRUE";
                        break;
                    case 6:
                        row[5] = "NA";
                        break;
                    default:
                        row = null;
                }
                return row;
            }).filter(x -> x != null).collect(Collectors.toMap(x -> x[2] + x[3], x -> x));

        File file = newPath.toFile();
        boolean exists = file.exists();

        // reset lines counter
        line.set(0);

        try {
            // lock path for current thread
            Utils.lock(newPath.toString());

            // collection of rows with data
            Collection<String[]> rows;

            if (exists) {
                rows = Files.lines(newPath, StandardCharsets.UTF_8)
                    .filter(x -> line.getAndIncrement() > 0 && null != x && !x.trim().isEmpty() && x.contains("|"))
                    .map(x -> {
                        String[] row = x.split("\\|");

                        for (int i = 0; i < row.length - 1; i++) {
                            row[i] = row[i + 1].trim();
                        }

                        String[] newRow = rowMap.get(row[2] + row[3]);

                        if (null != newRow) {
                            switch(newRow.length) {
                                case 5:
                                    // we trust/use the last trading date and delivery date from the LME exchange over
                                    // that of PRIME
                                    row[0] = newRow[0];
                                    row[1] = newRow[1];
                                    break;
                                case 6:
                                    // a special case when update of data is based on PRIME update
                                    row[4] = newRow[4];
                                default:
                            }
                        }
                        return row;
                    }).filter(x -> x != null).collect(Collectors.toList());
            } else {
                file.getParentFile().mkdirs(); // ensure that dirs are created

                rows = rowMap.values();
            }

            try (BufferedWriter bw = Files.newBufferedWriter(newPath, StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)) {
                bw.write(String.format("| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |%n"));

                rows.forEach(x -> {
                    try {
                        bw.write(String.format("| %s | %s | %s | %s | %S |%n", (Object[])x));
                    } catch (IOException e) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                            String.format("An exception '%s' occurred while writing data '%s' into the file '%s'",
                                e.getMessage(), Arrays.toString(x), newPath), e);
                   }
                });
            }
        } finally {
            Utils.unlock(newPath.toString());
        }
        return 0;
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#isSupported(ao.demo.pipeline.DataEntry)
     */
    @Override
    public boolean isSupported(DataEntry entry) {
        return "LME".equals(entry.getType()) && entry.getFile().toString().toLowerCase().endsWith(".txt");
    }

}