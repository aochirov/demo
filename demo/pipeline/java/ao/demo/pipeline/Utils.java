package ao.demo.pipeline;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The <code>Utils</code> is an utility class that plays role of container of utility methods that required by
 * main application.
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class Utils {

    /**
     * The common lock object that is used to make operation of registering new lock object atomic.
     */
    private final static Lock lock = new ReentrantLock();

    /**
     * The active cache of application's lock that used to control access to output files.
     * <p>
     * This map with locks is used because usage of Channel's locks is useless in current situation.
     */
    private final static Map<String, Lock> locks = new ConcurrentHashMap<>();

    /**
     * Locks the given path by current thread. Method base on {@link ReentrantLock} and follows it's
     * {@link ReentrantLock#lock()} logic.
     *
     * @param   path the path to lock by current thread.
     */
    public static void lock(String path) {
        Lock pathLock = locks.get(path);

        if (null == pathLock) {
            pathLock = makeLock(path); // create and add new lock object
        }
        // lock the given path to ensure that only current thread can work with given file
        pathLock.lock();
    }

    /**
     * Unlocks the given path by current thread. Method base on {@link ReentrantLock} and follows it's
     * {@link ReentrantLock#unlock()} logic.
     *
     * @param   path the path to unlock by current thread.
     */
    public static void unlock(String path) {
        Lock pathLock = locks.get(path);

        if (null == pathLock) {
            // nothing to unlock
            return;
        }
        pathLock.unlock();
    }

    /**
     * Creates and adds lock into map of locks.
     * <p>
     * This method is uses common {@link #lock} because making and adding new lock object is not atomic operation.
     *
     * @param   path the path that needed new lock object.
     */
    private static Lock makeLock(String path) {
        try {
            lock.lock();

            // we have to check if lock for given path already created
            Lock pathLock = locks.get(path);

            if (null == pathLock) { // if not we create it and add into map of locks
                pathLock = new ReentrantLock();

                // saving new lock object in the map of locks
                locks.put(path, pathLock);
            }
            return pathLock;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Creates an instance of class <code>Utils</code>.
     */
    private Utils() {
        // the private constructor to avoid instantiation of this utility class
    }

}