package ao.demo.pipeline;

/**
 * The <code>DataHandler</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public interface DataHandler {

    /**
     *
     * @param   entry
     * @return  a code of success of error as indicator of data handling operation success.
     *
     * @throws  Exception
     */
    public Integer handle(DataEntry entry) throws Exception;

    /**
     *
     * @param   entry the data entry to test.
     * @return  <code>true</code> if given data entry is supported and can be handled by this data handler.
     */
    public boolean isSupported(DataEntry entry);

}