package ao.demo.pipeline;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The <code>DataScanner</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class DataScanner {

    /**
     * The <code>DataScanner.DataFileVisitor</code> is TODO write correct comment
     *
     * @author  Andrey ochirov
     * @version 1.0
     */
    private static class DataFileVisitor extends SimpleFileVisitor<Path> {

        private final Logger logger;
        private final Map<Path, DataEntry> files;
        private final DataPipeline pipeline;

        /**
         * Creates an instance of class <code>DataScanner.DataFileVisitor</code>.
         *
         * @param   logger
         * @param   files
         * @param   pipeline
         */
        public DataFileVisitor(Logger logger, Map<Path, DataEntry> files, DataPipeline pipeline) {
            this.files = files;
            this.logger = logger;
            this.pipeline = pipeline;
        }

        /*
         * (non-Javadoc)
         * @see java.nio.file.FileVisitor#preVisitDirectory(java.lang.Object, java.nio.file.attribute.BasicFileAttributes)
         */
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            if ("archive".equalsIgnoreCase(dir.getFileName().toString()) ||
                    "output".equalsIgnoreCase(dir.getFileName().toString())) {
                return FileVisitResult.SKIP_SUBTREE;
            }
            return FileVisitResult.CONTINUE;
        }

        /*
         * (non-Javadoc)
         * @see java.nio.file.FileVisitor#visitFile(java.lang.Object, java.nio.file.attribute.BasicFileAttributes)
         */
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            DataEntry entry = files.get(file);

            if (null == entry) {
                files.put(file, entry = new DataEntry(file));
                pipeline.process(entry.list());

                logger.info(String.format("A new file '%s' is found and added into known files.", file.toString()));
            } else {
                if (!entry.checkSignature(file.toFile())) {
                    entry.updateSingature();

                    logger.info(String.format("A file '%s' was updated.", file.toString()));

                    if (!entry.isListed()) {
                        pipeline.process(entry.list());
                        logger.info(String.format("A file '%s' is listed for processing.", file.toString()));
                    }
                }
            }
            return FileVisitResult.CONTINUE;
        }

    }

    private Logger logger;
    private ScheduledExecutorService executor;

    private final Path root;
    private final Map<Path, DataEntry> knownFiles;
    private final DataPipeline pipeline;

    /**
     * Creates an instance of class <code>DataScanner</code>.
     *
     * @param   root the root path for all data files.
     * @param   pipeline
     * @param   logger
     */
    public DataScanner(Path root, DataPipeline pipeline, Logger logger) {
        this.root = root;
        knownFiles = new ConcurrentHashMap<>();
        this.pipeline = pipeline;
        this.logger = logger;
    }

    /**
     * Initializes data scanner.
     *
     * @param   context an application context for external resources.
     */
    public void create() {
        logger.info("Creating Data Scanner...");

        // initializing scheduler
        executor = Executors.newScheduledThreadPool(1, (Runnable task) -> {
                return new Thread(task, "DataScanner");
            });

        Runnable task = () -> {
            try {
                Files.walkFileTree(root, new DataFileVisitor(logger, knownFiles, pipeline));
            } catch (IOException e) {
               logger.log(Level.SEVERE,
                   String.format("An exception '%s' occurred while monitoring '%s' folder for updates", e.getMessage(),
                       root.toString()), e);
            }

            for (Path file : knownFiles.keySet()) {
                if (Files.notExists(file, LinkOption.NOFOLLOW_LINKS)) {
                    logger.info(String.format("A file '%s' has been removed from file system and will be removed from the "
                        + "list.", file.toString()));

                    DataEntry entry = knownFiles.remove(file);

                    if (null != entry && entry.isListed()) {
                        entry.delist();
                    }
                }
            }
        };

        logger.info("Starting Data Scanner service...");

        executor.scheduleAtFixedRate(task, 0, 5, TimeUnit.SECONDS);

        logger.info("Data Scanner has been successfully created.");
    }

    /**
     *
     * @param   context
     */
    public void destroy() {
        logger.info("Destroying Data Scanner...");

        try {
            executor.shutdown();
            executor.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            // do nothing...
        } finally {
            if (!executor.isTerminated()) {
                logger.info("Trying to stop Data Scanner forcibly...");
            }
            executor.shutdownNow();
        }

        knownFiles.clear();

        logger.info("Data Scanner has been successfully destroyed.");
        logger = null;
    }

}