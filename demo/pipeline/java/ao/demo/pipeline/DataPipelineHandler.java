package ao.demo.pipeline;

import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

/**
 * The <code>DataPipelineHandler</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class DataPipelineHandler implements DataHandler {

    /**
     * The <code>DataHandlerClassLoader</code> is TODO write correct comment
     *
     * @author  Andrey OCHIROV
     * @version 1.0
     */
    public class DataHandlerClassLoader extends ClassLoader {

        /**
         * Creates an instance of class <code>DataHandlerClassLoader</code>.
         *
         * @param   parent the parent class loader.
         */
        public DataHandlerClassLoader(ClassLoader parent) {
            super(parent);
        }

        /**
         * Loads the class with from specified array of bytes.
         *
         * @param   clazz the array of bytes.
         * @return  the resulting <tt>Class</tt> object created from the given array of bytes.
         */
        public Class<?> loadClass(byte[] clazz) {
            // a primitive way to get class name...
            try {
                // it will pass in case when name is missing ;D...
                return defineClass("", clazz, 0, clazz.length);
            } catch (NoClassDefFoundError e) {
                // in case of wrong name we can get correct package name from error message
                String name = e.getMessage().trim().substring(e.getMessage().trim().lastIndexOf(' '),
                    e.getMessage().lastIndexOf(')') - 1).replace('/', '.').trim();

                // let's try again...
                return defineClass(name, clazz, 0, clazz.length);
            }
        }

    }

    private final DataPipeline pipeline;

    /**
     * Creates an instance of class <code>DataPipelineHandler</code>.
     *
     * @param   pipeline the pipeline to manage.
     */
    public DataPipelineHandler(DataPipeline pipeline) {
        this.pipeline = pipeline;
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#handle(ao.demo.pipeline.DataEntry)
     */
    @Override
    public Integer handle(DataEntry entry) throws Exception {
        try {
            byte[] classBytes = Files.readAllBytes(entry.getFile());

            Class<?> clazz = new DataHandlerClassLoader(getClass().getClassLoader()).loadClass(classBytes);

            if (Modifier.isAbstract(clazz.getModifiers()) || clazz.isInterface() || clazz.isEnum()) {
                pipeline.logger.warning(String.format("An entry '%s' has not acceptable type. The entry class cannot "
                    + "be abstract, enum or interface.", entry.getFile()));
                return 1;
            }

            if (!DataHandler.class.isAssignableFrom(clazz)) {
                pipeline.logger.warning(String.format("A class '%s' from entry '%s' is not 'DataHandler' type class "
                    + "and cannot be used.", clazz.getName(), entry.getFile()));
                return 2;
            }

            if (getClass().getName().equals(clazz.getName())) {
                pipeline.logger.warning(String.format("A data pipeline handler cannot be replaced by the given class "
                    + "from entry '%s'. To update data pipeline class the application should be fully recompiled.",
                        entry.getFile()));
                return 3;
            }

            // trying to create an instance of new DataHandler with expected default constructor...
            DataHandler handler = (DataHandler)clazz.newInstance();

            try {
                pipeline.handlersLock.lock();

                List<DataHandler> handlers = new ArrayList<>();

                pipeline.handlers.forEach(h -> {
                    if (h.getClass().getName().equals(clazz.getName())) {
                        // we are trying replace existing DataHandler with new one
                        // ignore old handler and do not copy it into new collections of handlers
                        pipeline.logger.info(String.format("An old DataHandler '%s' instance will be unloaded and "
                            + "replaced with new one...", h.getClass().getName()));
                        return;
                    }

                    handlers.add(h);
                });

                handlers.add(handler);

                // replacing pipeline handlers here...
                pipeline.handlers = Collections.unmodifiableList(handlers);
            } finally {
                pipeline.handlersLock.unlock();
            }
        } catch (Throwable the) {
            pipeline.logger.log(Level.SEVERE, String.format("An exception '%s' occurred while loading Java Class for "
                + "entry '%s'", the.getMessage(), entry.getFile().toString()), the);

            return 4;
        }
        return 0;
    }

    /* (non-Javadoc)
     * @see ao.demo.pipeline.handler.DataHandler#isSupported(ao.demo.pipeline.DataEntry)
     */
    @Override
    public boolean isSupported(DataEntry entry) {
        return "system".equals(entry.getType().toLowerCase()) && entry.getFile().toString().endsWith(".class");
    }

}