package ao.demo.pipeline;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import ao.demo.pipeline.handler.LmeDataHandler;
import ao.demo.pipeline.handler.PrimeDataHandler;

/**
 * The <code>DataPipeline</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class DataPipeline {

    /**
     * The <code>DataPipelineTask</code> is TODO write correct comment
     *
     * @author  Andrey OCHIROV
     * @version 1.0
     */
    private class DataPipelineTask implements Callable<Integer> {

        private final DataEntry entry;

        /**
         * Creates an instance of class <code>DataPipelineTask</code>.
         *
         * @param   entry
         */
        public DataPipelineTask(DataEntry entry) {
            this.entry = entry;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public Integer call() {
            if (!entry.isListed()) {
                return 0; // entry has been delisted (i.e. file has been removed) and cannot be processed
            }

            int code = 0;

            for (DataHandler dh : handlers) {
                if (dh.isSupported(entry)) {
                    try {
                        // here better to have lock functionality to lock file while processing running...
                        logger.info(String.format("Starting processing of the entry '%s' by '%s'",
                            entry.getFile(), dh.getClass().getName()));

                        code = dh.handle(entry);

                        logger.info(String.format("Entry '%s' has been processed successfully by '%s'",
                            entry.getFile(), dh.getClass().getName()));

                        if (rootHandler == dh || !entry.getFile().toFile().exists()) {
                            // data entry for root handler is not archived and not removed or entry doesn't exists
                            break;
                        }

                        // calculating path for entry in the archive
                        Path path = entry.getFile();
                        Path archive = path.subpath(0, 1).resolve("archive")
                            .resolve(path.subpath(1, path.getNameCount()));
                        boolean outOfRange = true;

                        // ensure that folders are created for this entry
                        archive.toFile().getParentFile().mkdirs();

                        String archiveFileTemplate = path.getName(path.getNameCount() - 1) + ".%010d";

                        // making history for MAX_VALUE copies per each entry...
                        for (int i = 0; i < Integer.MAX_VALUE; i++) {
                            Path temp = archive.resolveSibling(String.format(archiveFileTemplate, i));

                            if (Files.notExists(temp)) {
                                try {
                                    Files.move(path, temp, StandardCopyOption.ATOMIC_MOVE);

                                    outOfRange = false;

                                    logger.info(String.format("Entry '%s' successfully moved to archive under new "
                                        + "name '%s'", path, temp));
                                } catch(Exception e) {
                                    logger.log(Level.SEVERE, String.format("An exception '%s' occurred while moving "
                                        + "entry file '%s' to archive under new name '%s'", e.getMessage(), path, temp),
                                            e);
                                }
                                break;
                            }
                        }

                        if (outOfRange) {
                            logger.info(String.format("Entry '%s' cannot be archived. Number of historical copies is "
                                + "more than %d", path, Integer.MAX_VALUE));
                        }
                    } catch (Exception e) {
                        logger.log(Level.SEVERE,
                            String.format("An exception '%s' occurred while processing entry '%s' by '%s'",
                                e.getMessage(), entry.getFile(), dh.getClass().getName()), e);

                        code = 1; // indicate an error...
                    } finally {
                        entry.delist();
                    }
                    break;
                }
            }
            return code;
        }

    }

    protected Logger logger;
    protected ExecutorService executor;
    protected List<DataHandler> handlers;
    protected final Lock handlersLock;

    private final DataHandler rootHandler;

    /**
     * Creates an instance of class <code>DataPipeline</code>.
     *
     * @param   logger the logger to use.
     */
    public DataPipeline(Logger logger) {
        rootHandler = new DataPipelineHandler(this); // a main handler
        handlersLock = new ReentrantLock();
        this.logger = logger;
    }

    /**
     *
     * @param   context
     */
    public void create() {
        logger.info("Creating Data Pipeline...");
        logger.info("Instantiating Data Handlers for data processing...");

        handlers = createHandlers();

        // add root handler to support of dynamic configuration
        handlers.add(rootHandler);

        // keeping one CPU for other activities (OS, IO, etc)
        int threads = (threads = Runtime.getRuntime().availableProcessors()) > 1 ? --threads : 1;

        logger.info(String.format("Configuring Data Pipeline to support %d concurrent handlers...", threads));

        executor = Executors.newFixedThreadPool(threads, new ThreadFactory() {

            private int counter;

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "DataPipeline-" + ++counter);
            }
        });
        logger.info("Data Pipeline has been created successfully.");
    }

    /**
     *
     * @param   context
     */
    public void destroy() {
        logger.info("Destroying Data Pipeline...");

        executor.shutdown();

        int timer = 0;
        // waiting while all files in a queue will be processed...
        while (!executor.isTerminated()) {
            try {
                Thread.sleep(1000);

                if (++timer % 10 == 0) {
                    logger.info(String.format("Waiting Data Pipeline for shutdown %04d sec...", timer));
                }
            } catch (Exception e) {}
        }
        logger.info("Data Pipeline has been successfully destroyed.");
        logger = null;
    }

    /**
     *
     * @param   entry
     * @return  ...
     */
    @SuppressWarnings("unchecked")
    public <T> Future<T> process(DataEntry entry) {
        return (Future<T>)executor.submit(new DataPipelineTask(entry));
    }

    /**
     * Creates an array of {@link DataHandler data handlers} that used by pipeline to handle
     * {@link DataEntry data entries} during runtime.
     *
     * @return
     */
    protected List<DataHandler> createHandlers() {
        return new ArrayList<>(Arrays.asList(new LmeDataHandler(), new PrimeDataHandler()));
    }

}