package ao.demo.pipeline;

import java.io.File;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import ao.demo.AbstractApplication;
import ao.demo.ApplicationError;

/**
 * The <code>PipelineApplication</code> TODO complete comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class PipelineApplication extends AbstractApplication {

    /**
     * Runs this application with the given arguments.
     *
     * @param   args the arguments for this application.
     */
    public static void main(String[] args) {
        new PipelineApplication().run(args);
    }

    private DataPipeline pipeline;
    private DataScanner scanner;

    /**
     * Creates an instance of class <code>PipelineApplication</code>.
     */
    public PipelineApplication() {
        super("Pipeline Application", "pipeline.application.log");

        hint = "<folder name with data files>";
    }

    /*
     * (non-Javadoc)
     * @see ao.demo.pipeline.AbstractApplication#destroy()
     */
    @Override
    protected void destroy() {
        if (null != scanner) {
            scanner.destroy();
        }

        try {
            // wait while queue will be processed
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {}

        if (null != pipeline) {
            pipeline.destroy();
        }

        super.destroy();
    }

    /*
     * (non-Javadoc)
     * @see ao.demo.pipeline.AbstractApplication#execute(java.lang.String[])
     */
    @Override
    protected void execute(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            for (boolean alive = true; alive;) {
                String line = sc.nextLine();

                alive = !"exit".equalsIgnoreCase(line.trim());
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see ao.demo.pipeline.AbstractApplication#init(java.lang.String[])
     */
    @Override
    protected String[] init(String[] params) {
        String dataName = null;

        if (null == params || 0 == params.length) {
            printError("A folder with data files must be specified");

            logger.info("Default name for the data folder 'data' will be used...");
            dataName = "data";
        } else {
            dataName = params[0];
        }

        File folder = new File(dataName);

        if (!folder.exists() || !folder.isDirectory()) {
            printErrorOnly(String.format("Specified folder '%s' with data files is incorrect: not exists or not directory. "
                + "Please check folder name and file system configuration", dataName));

            if (!folder.exists()) {
                if (!folder.mkdirs()) {
                    throw new ApplicationError(201);
                }
            } else {
                throw new ApplicationError(200);
            }
        } else {
            logger.info(String.format("A folder '%s' will be used as folder for data files for input and output.",
                folder.getAbsolutePath()));
        }

        // scanner writes to pipeline...
        scanner = new DataScanner(folder.toPath(), pipeline = new DataPipeline(logger), logger);

        pipeline.create();
        scanner.create();

        return super.init(params);
    }

    /*
     * (non-Javadoc)
     * @see ao.demo.pipeline.AbstractApplication#printHelpArguments()
     */
    @Override
    protected void printHelpArguments() {
        logger.info("    <data folder>\tthe name of the folder where data files are stored\n");
    }

}