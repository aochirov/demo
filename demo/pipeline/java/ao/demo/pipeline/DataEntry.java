package ao.demo.pipeline;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The <code>DataEntry</code> is TODO write correct comment
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public class DataEntry {

    private final Path file;
    private final String type;
    private final AtomicInteger listed;

    private String signature;

    /**
     * Creates an instance of class <code>DataEntry</code>.
     *
     * @param   file
     */
    public DataEntry(Path file) {
        this(file, file.toFile().length() + ":" + file.toFile().lastModified());
    }

    /**
     * Creates an instance of class <code>DataEntry</code>.
     *
     * @param   file
     * @param   signature
     */
    public DataEntry(Path file, String signature) {
        this.file = file;
        this.signature = signature;
        listed = new AtomicInteger(0);
        type = file.subpath(1, 2).getFileName().toString();
    }

    /**
     * Tests specified file against file from entry. If its size and last modified date not the same it returns
     * <code>false</code>, otherwise returns <code>true</code>.
     *
     * @param   file the file to test.
     * @return  <code>true</code> if files are equals.
     */
    public boolean checkSignature(File file) {
        return signature.equals(file.length() + ":" + file.lastModified());
    }

    /**
     *
     * @return  this instance of {@link DataEntry}.
     */
    public DataEntry delist() {
        listed.decrementAndGet();
        return this;
    }

    /**
     * @return 	the file
     */
    public Path getFile() {
        return file;
    }

    /**
     * @return 	the type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @return  <code>true</code> if this data entry is listed in processing queue; otherwise returns
     *          <code>false</code>
     */
    public boolean isListed() {
        return listed.get() > 0;
    }

    /**
     *
     * @return  this instance of {@link DataEntry}.
     */
    public DataEntry list() {
        listed.incrementAndGet();
        return this;
    }

    /**
     *
     */
    public void updateSingature() {
        signature = file.toFile().length() + ":" + file.toFile().lastModified();
    }

}