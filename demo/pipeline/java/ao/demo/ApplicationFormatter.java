package ao.demo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Print a brief summary of the {@link LogRecord} in a human readable format. The summary will typically be 1 line.
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 *
 * @see     Formatter
 */
public class ApplicationFormatter extends Formatter {

    /**
     * The date object. Used to reduce execution time.
     */
    private static final Date DATE = new Date();

    /**
     * The date format pattern for log record.
     */
    private static final String PATTERN = "{0,date} {0,time,hh:mm:ss.SSS}";

    /**
     * The array for one argument. Used to reduce execution time.
     */
    private final Object args[] = new Object[1];

    /**
     * The message formatter. Used to reduce execution time.
     */
    private MessageFormat formatter = new MessageFormat(PATTERN);

    /**
     * Creates an instance of class <code>ApplicationFormatter</code>.
     */
    public ApplicationFormatter() {
        super();
    }

    /**
     * Format the given log record and return the formatted string.
     * <p>
     * The resulting formatted {@link String} will normally include a localized and formated version of the LogRecord's
     * message field. The {@link Formatter#formatMessage(LogRecord)} convenience method can (optionally) be used to
     * localize and PATTERN the message field.
     *
     * @param   record the log record to be formatted.
     * @return  the formatted log record.
     */
    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        StringBuffer text = new StringBuffer();

        // minimize memory allocations here
        DATE.setTime(record.getMillis());
        args[0] = DATE;

        formatter.format(args, text, null);

        sb.append(text);
        sb.append(" ");

        // getting localized message here
        String message = formatMessage(record);

        String name = record.getLevel().getName();

        if (10 > name.length()) {
            for (int i = 10 - name.length(); i > 0; i--) {
                name = " ".concat(name);
            }
        }

        sb.append(name);
        sb.append(": ");
        sb.append(message);
        sb.append("\n");

        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();

            try (PrintWriter pw = new PrintWriter(sw)) {
                record.getThrown().printStackTrace(pw);
            }
            sb.append(sw.toString());
        }
        return sb.toString();
    }

}