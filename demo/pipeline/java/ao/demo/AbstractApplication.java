package ao.demo;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The <code>AbstractApplication</code> class is an Abstract Application that defines application design.
 *
 * @author  Andrey OCHIROV
 * @version 1.0
 */
public abstract class AbstractApplication {

    /**
     * The count of required parameters.
     */
    protected int count;

    /**
     * The hint for this application.
     */
    protected String hint;

    /**
     * The logger for this application.
     */
    protected Logger logger;
    protected Logger rootLogger;

    /**
     * The name of this application.
     */
    protected String name;

    /**
     * The prefix for options.
     */
    protected String prefix = "-";

    /**
     * Creates an instance of class <code>AbstractApplication</code>.
     */
    public AbstractApplication(String name, String log) {
        this.name = null == name ? getClass().getSimpleName() : name;
        logger = Logger.getLogger(getClass().getName());

        // configuring application's logger with console handler and formatter
        Handler handler = new ConsoleHandler();

        handler.setLevel(Level.ALL);
        handler.setFormatter(new ApplicationFormatter());

        rootLogger = logger;

        while (null != rootLogger.getParent()) {
            rootLogger = rootLogger.getParent();
        }

        // cleaning parent handlers
        for (Handler h : rootLogger.getHandlers()) {
            rootLogger.removeHandler(h);
        }

        rootLogger.setLevel(Level.ALL);
        rootLogger.addHandler(handler);

        // configuring application's logger with file handler and formatter
        File file = new File(log == null ? "application.log" : log); // default name for the log file if not specified

        if (file.exists() && file.isDirectory()) {
            printError("Invalid log file name \""
                + file.getAbsolutePath()
                + "\" has been specified...");
            throw new ApplicationError(1);
        }

        try {
            Handler fHandler = new FileHandler(file.getAbsolutePath());

            fHandler.setEncoding("UTF-8");
            fHandler.setFormatter(new ApplicationFormatter());

            rootLogger.addHandler(fHandler);
        } catch (SecurityException | IOException e) {
            printError("Cannot create logging file \""
                + file.getAbsolutePath()
                + "\"...");
            printError("An exception \""
                + e.getMessage()
                + "\" occurred while trying to create logging file \""
                + file.getAbsolutePath()
                + "\"...");
            throw new ApplicationError(2);
        }
    }

    /**
     * Returns the hint for this application. This hint is used in printing help information in command line.
     *
     * @return  the hint for this application.
     *
     * @see     #printHelp()
     */
    public String getHint() {
        return hint;
    }

    /**
     * Returns application's logger.
     *
     * @return the current logger of this application.
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Returns the name of this application.
     *
     * @return the name of this application.
     */
    public String getName() {
        return name;
    }

    /**
     * Destroys this application. All necessary activities to free resources, etc. should be placed into this method.
     */
    protected void destroy() {
    }

    /**
     * Executes this application with the given arguments.
     *
     * @param   args the arguments for this application.
     */
    protected abstract void execute(String[] args);

    /**
     * Initialize application with the given parameters. Method checks parameters for potential options and remove all
     * of them from parameters to keep only arguments.
     *
     * @param   params the parameters for this application.
     * @return  arguments for this application.
     */
    protected String[] init(String[] params) {
        return params;
    }

    /**
     * Prints specified error message.
     *
     * @param   message the error message to print.
     */
    protected void printError(String message, Object... args) {
        printErrorOnly(message, args);
        printHelp();
    }

    /**
     * Prints specified error message only.
     *
     * @param   message the error message to print.
     */
    protected void printErrorOnly(String message, Object... args) {
        logger.severe(String.format(message, args) + "\n");

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }
    }

    /**
     * Prints application's help on the screen.
     */
    protected void printHelp() {
        logger.info("Usage: " + name + " " + hint + "\n");
        logger.info("where arguments include:");

        printHelpArguments();
    }

    /**
     * Prints appliction's help on the screen fully dedicated to application's arguments.
     */
    protected abstract void printHelpArguments();

    /**
     * Prints application's intro on the screen.
     */
    protected void printIntro() {
        logger.info("DEMO " + name + " v1.0.\n");
        logger.info("NOTE: Type 'exit' and press Enter to exit application...\n");
    }

    /**
     * Runs application with the given parameteres.
     *
     * @param   params the parameters to use by application. Contains options and arguments.
     */
    protected void run(String[] params) {
        printIntro();

        int code = 0;

        try {
            if (params.length < count) { // minimum arguments count
                printError("Required arguments are missing...");
                return;
            }

            String[] args = init(params);

            execute(args);
        } catch (ApplicationError e) {
            code = e.getErrorCode();
        } catch (Exception e) {
            logger.log(Level.SEVERE, String.format("An unknown exception '%s' occurred while running application",
                e.getMessage()), e);
            code = 999; // unknown exception
        } finally {
            destroy();
        }

        // tell OS shell about execution result
        System.exit(code);
    }

}